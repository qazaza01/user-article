package com.qazaza.domain.enumeration;

/**
 * The Color enumeration.
 */
public enum Color {
    RED, GREEN, BLACK, WHITE, BLUE
}
